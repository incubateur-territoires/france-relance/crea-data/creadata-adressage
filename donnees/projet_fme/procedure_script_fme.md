-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/20 : DC / Procédure des scripts éxistants

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                      SCRIPTS FME : Procédure des scripts éxistants                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

1.**Export numéro** :

Le script C:\FME\Adressage\Export_projet_adresse\01_export_numero_to_shp.fmw permet d'exporter les numéros de la table projet adresse quand une commune décide de mettre à jour ses données adresses via la plateforme "Mes Adresses".

Les données sont envoyés vers un fichier shp qui est déposé ici : \\Copernic\production\SORTIE\ADRESSE-PROJET
Une fois le fichier déposé il faut le copier et le déplacer ici : [Sharepoint](https://atd16.sharepoint.com/:f:/s/Normalisationdeladressage/En8itMpAtVJBpwwAAD9niroBH4uRSBIxXpYAJsYOr9XJiA?e=wmMY8N) et le mettre dans le dossier de la commune.



2.**Suppression dans projet numéro** :

Une fois le shp déposé au bon endroit lancer le scrip : C:\FME\Adressage\Export_projet_adresse\02_projet_numero_delete.fmw
Il supprime tout les numéros de la commune dans la table projet_numero

3.**Suppression dans la Bal16** :

Une fois les données supprimés dans le projet, il faut supprimé les données de la commune qui sont dans la bal pour ne plus les pousser lors des mises à jour. Lancer le script : C:\FME\Adressage\Normalisation_adressage\21_xmapProjet2BAL_delete.fmw

4.**Intégration BAN** :

Le script FME : C:\FME\Adressage\BAN_to_Xmap.fmw permet d'intégrer dans la table ban, les données en provenance de la base adresse nationale.
Il créé les points d'adresse en fonction des coordonnées.
Ce script est lancé automatiquement tout les lundi.
