-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/21 : SL / Migration du fichier sur Git
-- 2021/05/25 : SL / Mise en forme des COMMENT
--                 . Correction du nom des triggers
-- 2021/05/26 : SL / Ajout du champ the_geom
-- 2021/06/02 : SL / Ajout de la vue v_projet_voie
--                 . Ajout de la vue v_geo_alert_voie_sans_nom
--                 . Ajout de la vue v_geo_alert_voie_sans_type

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                    Table géographique : Voie d'adresse                                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_adresse.projet_voie;

CREATE TABLE sditec_adresse.projet_voie
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    identifiant_externe varchar(20), --[ATD16] Identifiant unique de la voie, lien avec la table projet_numero (concaténation automatique)
    rivoli_atd varchar(4), --[ATD16] Pseudo code rivoli généré par l'ATD (automatique)
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(254), --[SIRAP] Identifiant SIRAP / Nom de la voie concaténée (automatique)
    date_creation timestamp, --[ATD16] Date de création de l'objet (automatique)
    date_maj timestamp, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    libelle varchar(254), --[ATD16] Libellé de la voie (avec particules)
    type_voie varchar(2), --[FK][ATD16] Type de voie, lien avec la table type_voie
    type_numerotation varchar(2), --[FK][ATD16] Type de numérotation établie dans la voie, lien avec la table type_numerotation
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_projet_voie PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE sditec_adresse.projet_voie OWNER TO sditecgrp;

GRANT ALL ON TABLE sditec_adresse.projet_voie TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_adresse.projet_voie IS 
    '[ATD16] Table géographique contenant les linéaires de voies nommées';

COMMENT ON COLUMN sditec_adresse.projet_voie.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN sditec_adresse.projet_voie.identifiant_externe IS 
    '[ATD16] Identifiant unique de la voie, lien avec la table projet_numero (concaténation automatique)';
COMMENT ON COLUMN sditec_adresse.projet_voie.rivoli_atd IS '[ATD16] Pseudo code rivoli généré par l''ATD (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_voie.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN sditec_adresse.projet_voie.ident IS '[SIRAP] Identifiant SIRAP / Nom de la voie concaténée (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_voie.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_voie.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_voie.libelle IS '[ATD16] Libellé de la voie (avec particules)';
COMMENT ON COLUMN sditec_adresse.projet_voie.type_voie IS 
    '[FK][ATD16] Type de voie, lien avec la table type_voie';
COMMENT ON COLUMN sditec_adresse.projet_voie.type_numerotation IS 
    '[FK][ATD16] Type de numérotation établie dans la voie, lien avec la table type_numerotation';
COMMENT ON COLUMN sditec_adresse.projet_voie.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                  v_projet_voie : vue permettant une lecture de la table complète en flux                                   ###
-- ##################################################################################################################################################

-- DROP VIEW sditec_adresse.v_projet_voie;

CREATE OR REPLACE VIEW sditec_adresse.v_projet_voie 
    AS 
    SELECT 
        a.gid,
        a.identifiant_externe,
        a.insee,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.libelle,
        a.type_voie,
        c.libelle AS type_voie_libelle,
        a.type_numerotation,
        b.libelle AS type_num_libelle,
        a.rivoli_atd,
        a.the_geom
    FROM sditec_adresse.projet_voie a
    LEFT JOIN sditec_adresse.type_numerotation b ON a.type_numerotation::text = b.code::text -- Jointure avec la liste des types de numérotation
    LEFT JOIN sditec_adresse.type_voie c ON a.type_voie::text = c.code::text; -- Jointure avec la liste des types de voie

ALTER TABLE sditec_adresse.v_projet_voie OWNER TO sditecgrp;

COMMENT ON VIEW sditec_adresse.v_projet_voie IS 
    '[ATD16] Vue permettant une lecture de la table complète en flux (consultation cadastrale ou diffusion publique)';
    
    
-- ##################################################################################################################################################
-- ###                             v_geo_alert_voie_sans_nom : vue permettant de lister les voies n'ayant pas de nom                              ###
-- ##################################################################################################################################################

-- DROP VIEW sditec_adresse.v_geo_alert_voie_sans_nom;

CREATE OR REPLACE VIEW sditec_adresse.v_geo_alert_voie_sans_nom 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        a.libelle,
        c.libelle AS type_voie_libelle,
        b.libelle AS type_num_libelle,
        a.the_geom
   FROM sditec_adresse.projet_voie a
   LEFT JOIN sditec_adresse.type_numerotation b ON a.type_numerotation::text = b.code::text -- Jointure avec la liste des types de numérotation
   LEFT JOIN sditec_adresse.type_voie c ON a.type_voie::text = c.code::text -- Jointure avec la liste des types de voie
   WHERE a.libelle IS NULL -- Lorsque le libellé de la voie est NULL
   AND a.type_voie != '26' AND a.type_voie != '27' AND a.type_voie != '28'; 
   -- Et que le type de voie est différent de GRAND RUE, GRANDE RUE et GRAND'RUE

ALTER TABLE sditec_adresse.v_geo_alert_voie_sans_nom OWNER TO sditecgrp;

COMMENT ON VIEW sditec_adresse.v_geo_alert_voie_sans_nom IS '[ATD16] Vue permettant de lister les voies n''ayant pas de nom';


-- ##################################################################################################################################################
-- ###                             v_geo_alert_voie_sans_type : vue permettant de lister les voies n'ayant pas de type                              ###
-- ##################################################################################################################################################

-- DROP VIEW sditec_adresse.v_geo_alert_voie_sans_type;

CREATE OR REPLACE VIEW sditec_adresse.v_geo_alert_voie_sans_type 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        a.libelle,
        c.libelle AS type_voie_libelle,
        b.libelle AS type_num_libelle,
        a.the_geom
   FROM sditec_adresse.projet_voie a
   LEFT JOIN sditec_adresse.type_numerotation b ON a.type_numerotation::text = b.code::text -- Jointure avec la liste des types de numérotation
   LEFT JOIN sditec_adresse.type_voie c ON a.type_voie::text = c.code::text -- Jointure avec la liste des types de voie
   WHERE c.libelle IS NULL OR c.libelle = ''; -- Lorsque le libellé du type de voie est NULL ou vide.

ALTER TABLE sditec_adresse.v_geo_alert_voie_sans_type OWNER TO sditecgrp;

COMMENT ON VIEW sditec_adresse.v_geo_alert_voie_sans_type IS '[ATD16] Vue permettant de lister les voies n''ayant pas de type';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER update_customer_modtime_created_voie ON sditec_adresse.projet_voie;
-- DROP TRIGGER t_before_i_init_date_creation ON sditec_adresse.projet_voie;

CREATE TRIGGER update_customer_modtime_created_voie
-- CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON sditec_adresse.projet_voie
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.update_created_column();
    -- EXECUTE PROCEDURE sditec_adresse.f_date_creation();
    
COMMENT ON TRIGGER update_customer_modtime_created_voie ON sditec_adresse.projet_voie IS 
-- COMMENT ON TRIGGER t_before_i_init_date_creation ON sditec_adresse.projet_voie IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER update_customer_modtime_modified_voie ON sditec_adresse.projet_voie;
-- DROP TRIGGER t_before_u_date_maj ON sditec_adresse.projet_voie;

CREATE TRIGGER update_customer_modtime_modified_voie
-- CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON sditec_adresse.projet_voie
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.update_modified_column();
    -- EXECUTE PROCEDURE sditec_adresse.f_date_maj();
    
COMMENT ON TRIGGER update_customer_modtime_modified_voie ON sditec_adresse.projet_voie IS 
-- COMMENT ON TRIGGER t_before_u_date_maj ON sditec_adresse.projet_voie IS
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

