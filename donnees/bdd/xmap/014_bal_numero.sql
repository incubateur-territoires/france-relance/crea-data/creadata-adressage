-- ################################################################# SUIVI CODE SQL #################################################################

-- 2023/01/31 : DC / Création de la table

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Table géographique : BAL                                                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################
-- Table: sditec_adresse.bal_numero

-- DROP TABLE sditec_adresse.bal_numero;

CREATE TABLE IF NOT EXISTS sditec_adresse.bal_numero
(
    gid serial NOT NULL,
    insee character varying(20),
    ident character varying(40),
    cle_interop character varying(30),
    uid_adresse character varying(254),
    voie_nom character varying(254),
    numero integer,
    suffixe character varying(254),
    commune_nom character varying(254),
    "position" character varying(254),
    x double precision,
    y double precision,
    "long" numeric(8,7),
    lat double precision,
    source character varying(254),
    the_geom geometry(Point),
    date_der_maj date,
    commune_deleguee_insee character varying(5),
    commune_deleguee_nom character varying(50),
    lieudit_complement_nom character varying(256),
    cad_parcelles character varying(256),
    certification_commune character varying(1),
    CONSTRAINT pk_bal_numero PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE sditec_adresse.bal_numero
    OWNER to sditecgrp;

GRANT ALL ON TABLE sditec_adresse.bal_numero TO sditecgrp;

GRANT ALL ON TABLE sditec_adresse.bal_numero TO simapdatagrp;
-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_adresse.bal_numero IS '[AITF] Base adresse local de la Charente. Les numéros sont récupérer via un script FME de la table 
projet_numéro quand une commune à finie son adressage. La Bal16 est visible sur le prtail public, dans concultation cadastrales mais aussi
 dans Normalisation de l''adressage. Cette table est basée sur le format BAL1.3 préconisé par l''AITF';

COMMENT ON COLUMN sditec_adresse.bal_numero.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN sditec_adresse.bal_numero.insee IS '[AITF] Code insee de la commune';
COMMENT ON COLUMN sditec_adresse.bal_numero.ident IS '[ATD16] Champ vide';
COMMENT ON COLUMN sditec_adresse.bal_numero.cle_interop IS '[AITF] La clé d''interopérabilité du numéro';
COMMENT ON COLUMN sditec_adresse.bal_numero.uid_adresse IS '[AITF] [AITF] Champ vide';
COMMENT ON COLUMN sditec_adresse.bal_numero.voie_nom IS '[AITF] Nom de la voie de l''adresse';
COMMENT ON COLUMN sditec_adresse.bal_numero.numero IS '[AITF]Numéro de l''adresse';
COMMENT ON COLUMN sditec_adresse.bal_numero.suffixe IS '[AITF] Indice de répétition(bis, ter...)';
COMMENT ON COLUMN sditec_adresse.bal_numero.commune_nom IS '[AITF] Nom de la commune';
COMMENT ON COLUMN sditec_adresse.bal_numero.position IS '[AITF] Emplacement du numéro sur la parcelle';
COMMENT ON COLUMN sditec_adresse.bal_numero.x IS '[AITF] coordonnée x en projection Lambert 93';
COMMENT ON COLUMN sditec_adresse.bal_numero.y IS '[AITF] coordonnée y en projection Lambert 93';
COMMENT ON COLUMN sditec_adresse.bal_numero.long IS '[AITF] Longitude';
COMMENT ON COLUMN sditec_adresse.bal_numero.lat IS '[AITF] Latitude';
COMMENT ON COLUMN sditec_adresse.bal_numero.source IS '[AITF] Créateur de la donnée';
COMMENT ON COLUMN sditec_adresse.bal_numero.the_geom IS '[AITF] Géometrie de l''objet';
COMMENT ON COLUMN sditec_adresse.bal_numero.date_der_maj IS '[AITF]Date de la dernière mise à jour';
COMMENT ON COLUMN sditec_adresse.bal_numero.commune_deleguee_insee IS 'Code insee de la commune déléguée';
COMMENT ON COLUMN sditec_adresse.bal_numero.commune_deleguee_nom IS '[AITF] Nom de la commune déléguée';
COMMENT ON COLUMN sditec_adresse.bal_numero.lieudit_complement_nom IS '[AITF] Nom du lieu-dit';
COMMENT ON COLUMN sditec_adresse.bal_numero.cad_parcelles IS '[AITF] Référence cadastrale de la parcelle';
COMMENT ON COLUMN sditec_adresse.bal_numero.certification_commune IS '[AITF] Numéro certifié par la commune ';

-- ############################################################ Index##################### ###########################################################

-- DROP INDEX sditec_adresse.bal_numero_insee;

CREATE INDEX bal_numero_insee
    ON sditec_adresse.bal_numero USING btree
    (insee ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: bal_numero_the_geom_gist

-- DROP INDEX sditec_adresse.bal_numero_the_geom_gist;

CREATE INDEX bal_numero_the_geom_gist
    ON sditec_adresse.bal_numero USING gist
    (the_geom)
    TABLESPACE pg_default;