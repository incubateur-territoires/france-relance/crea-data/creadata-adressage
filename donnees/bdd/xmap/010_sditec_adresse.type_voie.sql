-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/25 : SL / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table non géographique : Domaine de valeur du type de voie                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_adresse.type_voie

CREATE TABLE sditec_adresse.type_voie 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du type de voie
    libelle varchar(254), --[ATD16] Libellé du type de voie
    abreviation varchar(254), --[ATD16] Abréviation du type de voie
    CONSTRAINT pk_type_voie PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE sditec_adresse.type_voie OWNER TO sditecgrp;

GRANT ALL ON TABLE sditec_adresse.type_voie TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_adresse.type_voie IS '[ATD16] Domaine de valeur des codes du type de voie';

COMMENT ON COLUMN sditec_adresse.type_voie.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN sditec_adresse.type_voie.code IS '[ATD16] Code du type de voie';
COMMENT ON COLUMN sditec_adresse.type_voie.libelle IS '[ATD16] Libellé du type de voie';
COMMENT ON COLUMN sditec_adresse.type_voie.abreviation IS '[ATD16] Abréviation du type de voie';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO sditec_adresse.type_voie 
    (code, libelle, abreviation)
VALUES
    ('00', '', ''),
    ('01', 'ALLÉE', 'ALL'),
    ('02', 'AVENUE', 'AV'),
    ('03', 'BOULEVARD', 'BD'),
    ('04', 'CHEMIN', 'CHE'),
    ('05', 'COURS', 'CRS'),
    ('06', 'IMPASSE', 'IMP'),
    ('07', 'PASSAGE', 'PAS'),
    ('08', 'PÉRIPHÉRIQUE', 'PERI'),
    ('09', 'PLACE', 'PL'),
    ('10', 'QUAI', 'QU'),
    ('11', 'ROUTE', 'RTE'),
    ('12', 'RUE', 'R'),
    ('13', 'RUELLE', 'RLE'),
    ('14', 'SQUARE', 'SQ'),
    ('15', 'ESPACE', 'ESPA'),
    ('16', 'PROMENADE', 'PROM'),
    ('17', 'LOTISSEMENT', 'LOT'),
    ('18', 'GIRATOIRE', 'GIRATOIRE'),
    ('19', 'ROCADE', 'ROC'),
    ('20', 'ZONE', 'ZONE'),
    ('21', 'RAMPE', 'RPE'),
    ('22', 'SENTIER', 'SEN'),
    ('23', 'VOIE', 'VOI'),
    ('24', 'ZA', 'ZA'),
    ('25', 'CONTRE-ALLÉE', 'CONTRE-ALLÉE'),
    ('26', 'GRAND RUE', 'GR'),
    ('27', 'GRANDE RUE', 'GR'),
    ('28', 'GRAND''RUE', 'GR'),
    ('29', 'VENELLE', 'VEN'),
    ('30', 'RÉSIDENCE', 'RES'),
    ('31', 'PARC', 'PARC'),
    ('32', 'PARVIS', 'PRV'),
    ('33', 'QUARTIER', 'QUA'),
    ('34', 'FAUBOURG', 'FG'),
    ('35', 'COTE', 'COTE'),
    ('36', 'RUETTE', 'RTTE');

