-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/26 : SL / Migration du fichier sur Git
-- 2021/06/02 : SL / Ajout de la vue v_projet_numero
--                 . Ajout de la vue v_geo_alert_num_sans_cp
--                 . Ajout de la vue v_geo_alert_num_sans_insee
--                 . Ajout de la vue v_geo_alert_num_sans_parcelle
--                 . Ajout de la vue v_geo_alert_num_sans_voie
--                 . Ajout de la vue v_geo_alert_sans_numero

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                   Table géographique : Numéro d'adresse                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_adresse.projet_numero;

CREATE TABLE sditec_adresse.projet_numero
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(254), --[SIRAP] Identifiant SIRAP / Vide ici
    date_creation timestamp, --[ATD16] Date de création de l'objet (automatique)
    date_maj timestamp, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    cle_interop varchar(30), --[AITF] Clé d'intéropérabilité pour l'échange de données : `insee_fantoir_numero_indice` (automatique)
    numero integer, --[ATD16] Numéro d'adresse
    indice varchar(254), --[ATD16] Indice de répétition
    code_postal varchar(20), --[ATD16] Code postal de l'adresse
    insee_voie varchar(6), --[PK][ATD16] Code INSEE à 6 chiffres, clé étrangère de la table insee_voie, permet de créer une liste liée dans X'MAP
    id_externe_voie varchar(20), --[PK][ATD16] Identifiant de la voie d'adresse, clé étrangère de la table projet_voie
    nom_ld character varying(256), --[ATD16] Nom du lieu-dit historique ou complémentaire
    parcelle character varying(256), --[ATD16] Référence cadastrale : `insee(3)_quartier(3)_section(2)_numéro(4)` (automatique)
    nom_prop character varying(256), --[ATD16] Nom du premier ayant-droit (automatique)
    nom_occupant varchar(100), --[ATD16] Nom de l'occupant lorsqu'il est différent du propriétaire
    commune_deleguee_insee varchar(5), --[AITF] Code INSEE de la commune déléguée sur 5 chiffres (automatique)
    commune_deleguee_nom varchar(50), --[AITF] Nom de la commune déléguée (automatique)
    x_cc46 double precision, --[ATD16] Coordonnée x en cc46 (automatique)
    y_cc46 double precision, --[ATD16] Coordonnée y en cc46 (automatique)
    x_wgs84 double precision, --[AITF] Coordonnée x (longitude) en wgs84 (automatique)
    y_wgs84 double precision, --[AITF] Coordonnée y (latitude) en wgs84 (automatique)
    x double precision, --[AITF] Coordonnée x en lambert 93 (automatique)
    y double precision, --[AITF] Coordonnée y en lambert 93 (automatique)
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_projet_numero PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE sditec_adresse.projet_numero OWNER TO sditecgrp;

GRANT ALL ON TABLE sditec_adresse.projet_numero TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_adresse.projet_numero IS 
    '[ATD16] Table géographique contenant les numéros d''adresse constructible dans X''MAP';

COMMENT ON COLUMN sditec_adresse.projet_numero.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN sditec_adresse.projet_numero.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN sditec_adresse.projet_numero.ident IS '[SIRAP] Identifiant SIRAP / Vide ici';
COMMENT ON COLUMN sditec_adresse.projet_numero.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.cle_interop IS 
    '[AITF] Clé d''intéropérabilité pour l''échange de données : `insee_fantoir_numero_indice` (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.numero IS '[ATD16] Numéro d''adresse';
COMMENT ON COLUMN sditec_adresse.projet_numero.indice IS '[ATD16] Indice de répétition';
COMMENT ON COLUMN sditec_adresse.projet_numero.code_postal IS '[ATD16] Code postal de l''adresse';
COMMENT ON COLUMN sditec_adresse.projet_numero.insee_voie IS '[PK][ATD16] Code INSEE à 6 chiffres, clé étrangère de la table insee_voie';
COMMENT ON COLUMN sditec_adresse.projet_numero.id_externe_voie IS 
    '[PK][ATD16] Identifiant de la voie d''adresse, clé étrangère de la table projet_voie';
COMMENT ON COLUMN sditec_adresse.projet_numero.nom_ld IS '[ATD16] Nom du lieu-dit historique ou complémentaire';
COMMENT ON COLUMN sditec_adresse.projet_numero.parcelle IS 
    '[ATD16] Référence cadastrale : `insee(3)_quartier(3)_section(2)_numéro(4)` (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.nom_prop IS '[ATD16] Nom du premier ayant-droit (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.nom_occupant IS '[ATD16] Nom de l''occupant lorsqu''il est différent du propriétaire';
COMMENT ON COLUMN sditec_adresse.projet_numero.commune_deleguee_insee IS '[AITF] Code INSEE de la commune déléguée sur 5 chiffres (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.commune_deleguee_nom IS '[AITF] Nom de la commune déléguée (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.x_cc46 IS '[ATD16] Coordonnée x en cc46 (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.y_cc46 IS '[ATD16] Coordonnée y en cc46 (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.x_wgs84 IS '[AITF] Coordonnée x (longitude) en wgs84 (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.y_wgs84 IS '[AITF] Coordonnée y (latitude) en wgs84 (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.x IS '[AITF] Coordonnée x en lambert 93 (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.y IS '[AITF] Coordonnée y en lambert 93 (automatique)';
COMMENT ON COLUMN sditec_adresse.projet_numero.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                 v_projet_numero : vue permettant une lecture de la table complète en flux                                  ###
-- ##################################################################################################################################################

-- DROP VIEW sditec_adresse.v_projet_numero;

CREATE OR REPLACE VIEW sditec_adresse.v_projet_numero 
    AS 
    SELECT 
        a.gid, 
        a.cle_interop,
        a.insee,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.numero,
        a.indice,
        a.id_externe_voie,
        b.ident AS nom_voie,
        a.nom_ld,
        a.code_postal,
        a.insee_voie, -- Dans X'MAP, permet de faire une liste liée et de voir les voies situées dans la commune sélectionnée
        c.libelle AS commune,
        a.parcelle,
        a.nom_prop,
        a.nom_occupant,
        a.commune_deleguee_insee,
        a.commune_deleguee_nom,
        a.x_cc46,
        a.y_cc46,
        a.x_wgs84,
        a.y_wgs84,
        a.x,
        a.y,
        a.the_geom
    FROM sditec_adresse.projet_numero a
    LEFT JOIN sditec_adresse.projet_voie b ON a.id_externe_voie::text = b.identifiant_externe::text -- Jointure avec la liste des voies
    LEFT JOIN sditec_adresse.insee_voie c ON a.insee::text = c.insee::text; -- Jointure avec la liste des communes

ALTER TABLE sditec_adresse.v_projet_numero OWNER TO sditecgrp;

COMMENT ON VIEW sditec_adresse.v_projet_numero IS 
    '[ATD16] Vue permettant une lecture de la table complète en flux (consultation cadastrale ou diffusion publique)';


-- ##################################################################################################################################################
-- ###                    v_geo_alert_num_sans_cp : vue permettant de lister les numéros d'adresse n'ayant pas de code postal                    ###
-- ##################################################################################################################################################

-- DROP VIEW sditec_adresse.v_geo_alert_num_sans_cp;

CREATE OR REPLACE VIEW sditec_adresse.v_geo_alert_num_sans_cp 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        a.numero,
        a.indice,
        b.ident AS nom_voie,
        a.nom_ld,
        a.code_postal,
        c.libelle AS commune,
        a.parcelle,
        a.the_geom
    FROM sditec_adresse.projet_numero a
    LEFT JOIN sditec_adresse.projet_voie b ON a.id_externe_voie::text = b.identifiant_externe::text -- Jointure avec la liste des voies
    LEFT JOIN sditec_adresse.insee_voie c ON a.insee::text = c.insee::text -- Jointure avec la liste des communes
    WHERE a.code_postal IS NULL; -- Lorsque le champ code-postal est NULL

ALTER TABLE sditec_adresse.v_geo_alert_num_sans_cp OWNER TO sditecgrp;

COMMENT ON VIEW sditec_adresse.v_geo_alert_num_sans_cp IS '[ATD16] Vue permettant de lister les numéros d''adresse n''ayant pas de code postal';


-- ##################################################################################################################################################
-- ###                   v_geo_alert_num_sans_insee : vue permettant de lister les numéros d'adresse n'ayant pas de code INSEE                   ###
-- ##################################################################################################################################################

-- DROP VIEW sditec_adresse.v_geo_alert_num_sans_insee;

CREATE OR REPLACE VIEW sditec_adresse.v_geo_alert_num_sans_insee 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        a.the_geom
    FROM sditec_adresse.projet_numero a
    WHERE a.insee IS NULL; -- Lorsque le champ insee est NULL

ALTER TABLE sditec_adresse.v_geo_alert_num_sans_insee OWNER TO sditecgrp;

COMMENT ON VIEW sditec_adresse.v_geo_alert_num_sans_insee IS '[ATD16] Vue permettant de lister les numéros d''adresse n''ayant pas de code INSEE';


-- ##################################################################################################################################################
-- ###             v_geo_alert_num_sans_parcelle : vue permettant de lister les numéros d'adresse n'étant pas situés sur une parcelle             ###
-- ##################################################################################################################################################

-- DROP VIEW sditec_adresse.v_geo_alert_num_sans_parcelle;

CREATE OR REPLACE VIEW sditec_adresse.v_geo_alert_num_sans_parcelle 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        a.numero,
        a.indice,
        b.ident AS nom_voie,
        a.nom_ld,
        a.code_postal,
        c.libelle AS commune,
        a.parcelle,
        a.the_geom
    FROM sditec_adresse.projet_numero a
    LEFT JOIN sditec_adresse.projet_voie b ON a.id_externe_voie::text = b.identifiant_externe::text -- Jointure avec la liste des voies
    LEFT JOIN sditec_adresse.insee_voie c ON a.insee::text = c.insee::text -- Jointure avec la liste des communes
    WHERE a.parcelle IS NULL; -- Lorsque le champ parcelle est NULL

ALTER TABLE sditec_adresse.v_geo_alert_num_sans_parcelle OWNER TO sditecgrp;

COMMENT ON VIEW sditec_adresse.v_geo_alert_num_sans_parcelle IS 
    '[ATD16] Vue permettant de lister les numéros d''adresse n''étant pas situés sur une parcelle';
    

-- ##################################################################################################################################################
-- ###                    v_geo_alert_num_sans_voie : vue permettant de lister les numéros d'adresse non rattachés à une voie                     ###
-- ##################################################################################################################################################

-- DROP VIEW sditec_adresse.v_geo_alert_num_sans_voie;

CREATE OR REPLACE VIEW sditec_adresse.v_geo_alert_num_sans_voie 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        a.numero,
        a.indice,
        a.id_externe_voie,
        b.ident AS nom_voie,
        a.nom_ld,
        a.code_postal,
        c.libelle AS commune,
        a.parcelle,
        a.the_geom
    FROM sditec_adresse.projet_numero a
    LEFT JOIN sditec_adresse.projet_voie b ON a.id_externe_voie::text = b.identifiant_externe::text -- Jointure avec la liste des voies
    LEFT JOIN sditec_adresse.insee_voie c ON a.insee::text = c.insee::text -- Jointure avec la liste des communes
    WHERE a.id_externe_voie IS NULL; -- Lorsque le champ id_externe_voie (clé étrangère de projet_voie) est NULL

ALTER TABLE sditec_adresse.v_geo_alert_num_sans_voie OWNER TO sditecgrp;

COMMENT ON VIEW sditec_adresse.v_geo_alert_num_sans_voie IS '[ATD16] Vue permettant de lister les numéros d''adresse non rattachés à une voie';


-- ##################################################################################################################################################
-- ###                           v_geo_alert_sans_numero : vue permettant de lister les numéros d'adresse sans numéro                             ###
-- ##################################################################################################################################################

-- DROP VIEW sditec_adresse.v_geo_alert_sans_numero;

CREATE OR REPLACE VIEW sditec_adresse.v_geo_alert_sans_numero 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        a.numero,
        a.indice,
        b.ident AS nom_voie,
        a.nom_ld,
        a.code_postal,
        c.libelle AS commune,
        a.parcelle,
        a.the_geom
    FROM sditec_adresse.projet_numero a
    LEFT JOIN sditec_adresse.projet_voie b ON a.id_externe_voie::text = b.identifiant_externe::text -- Jointure avec la liste des voies
    LEFT JOIN sditec_adresse.insee_voie c ON a.insee::text = c.insee::text -- Jointure avec la liste des communes
    WHERE a.numero IS NULL; -- Lorsque le champ numero est NULL

ALTER TABLE sditec_adresse.v_geo_alert_sans_numero OWNER TO sditecgrp;

COMMENT ON VIEW sditec_adresse.v_geo_alert_sans_numero IS '[ATD16] Vue permettant de lister les numéros d''adresse sans numéro';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER update_customer_modtime_created_voie ON sditec_adresse.projet_numero;
-- DROP TRIGGER t_before_i_init_date_creation ON sditec_adresse.projet_numero;

CREATE TRIGGER update_customer_modtime_created_voie
-- CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON sditec_adresse.projet_numero
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.update_created_column();
    -- EXECUTE PROCEDURE sditec_adresse.f_date_creation();
    
COMMENT ON TRIGGER update_customer_modtime_created_voie ON sditec_adresse.projet_numero IS 
-- COMMENT ON TRIGGER t_before_i_init_date_creation ON sditec_adresse.projet_numero IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER update_customer_modtime_modified_voie ON sditec_adresse.projet_numero;
-- DROP TRIGGER t_before_u_date_maj ON sditec_adresse.projet_numero;

CREATE TRIGGER update_customer_modtime_modified_voie
-- CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON sditec_adresse.projet_numero
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.update_modified_column();
    -- EXECUTE PROCEDURE sditec_adresse.f_date_maj();
    
COMMENT ON TRIGGER update_customer_modtime_modified_voie ON sditec_adresse.projet_numero IS 
-- COMMENT ON TRIGGER t_before_u_date_maj ON sditec_adresse.projet_numero IS
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

