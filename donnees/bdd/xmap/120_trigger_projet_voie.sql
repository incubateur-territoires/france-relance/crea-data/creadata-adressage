-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/26 : SL / Migration du fichier sur Git
--                 . Ajout de la fonction concat_ident() et du trigger associé concat_ident_trigger
--                 . Ajout de la fonction majuscule_projet_voie() et du trigger associé majuscule_projet_voie_trigger
--                 . Ajout de la fonction trim_ident_projet_voie() et du trigger associé trim_ident_projet_voie_trigger
--                 . Ajout de la fonction verif_voie_unique() et du trigger associé verif_voie_unique_trigger
--                 . Ajout de la fonction concat_identifiant_externe() et du trigger associé concat_identifiant_externe_trigger
--                 . Ajout de la fonction f_increment_rivoli_atd() et du trigger associé t_before_i_increment_rivoli_atd
-- 2021/05/27 : SL / Ajout de la fonction maj_cle_interop_fantoir() et du trigger associé maj_cle_interop_fantoir
--                 . Ajout de la fonction maj_id_externe_voie_syncro() et du trigger associé maj_id_externe_voie_syncro
--                 . Ajout de la fonction f_update_id_externe_voie_suppr_voie() et du trigger associé t_after_d_voie_adresse_maj_pt_adresse

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Fonctions triggers et triggers spécifiques à la table projet_voie                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                      Incrémentation du champ rivoli_atd sous la forme 'x001, x002...'                                      ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.f_increment_rivoli_atd();

CREATE OR REPLACE FUNCTION sditec_adresse.f_increment_rivoli_atd()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF -- Si le nombre total de voies de la commune sur laquelle une nouvelle est créée est égale à 0 alors
        (SELECT COUNT(*) FROM sditec_adresse.projet_voie WHERE insee = NEW.insee) = 0 THEN
        NEW.rivoli_atd := 'x001'; -- rivoli_atd prend la valeur 'x001'
    ELSE -- Sinon
        NEW.rivoli_atd := 
            CONCAT -- rivoli_atd prend la valeur d'une concaténation
                (
                    'x', -- De x 
                    LPAD -- Et avec la valeur générée par le LPAD (incrémentation)
                        ( 
                            (
                                SELECT(MAX(RIGHT(rivoli_atd,3))::integer) -- On sélectionne la valeur la plus haute du champ rivoli_atd
                                    +1 -- À laquelle on rajoute 1
                                FROM sditec_adresse.projet_voie -- De la table projet_voie
                                WHERE NEW.insee = insee --Où le code INSEE correspond à la valeur du code INSEE de l'objet créé
                                AND rivoli_atd LIKE 'x%' -- Et parmi celles qui commencent par x
                            )::text,
                            3,
                            '0'::text -- Fonction LPAD permettant de rajouter des 0 devant le résultat pour arriver à 3 caractères
                        )
                );
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.f_increment_rivoli_atd() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.f_increment_rivoli_atd() IS 
    'Fonction trigger permettant l''incrémentation du champ rivoli_atd sous la forme ''x001, x002...''';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_i_increment_rivoli_atd 
    BEFORE INSERT
    ON sditec_adresse.projet_voie 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.f_increment_rivoli_atd();
    
COMMENT ON TRIGGER t_before_i_increment_rivoli_atd ON sditec_adresse.projet_voie IS 
    'Trigger déclenchant la fonction permettant l''incrémentation du champ rivoli_atd sous la forme ''x001, x002...''';


-- ##################################################################################################################################################
-- ###         Bloquer la création d'une voie dans la table projet_voie lorsque celle-ci a les mêmes valeurs (insee, type_voie, libelle)          ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.verif_voie_unique();

CREATE OR REPLACE FUNCTION sditec_adresse.verif_voie_unique()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    existedeja varchar(254); --Variable existedeja
    
BEGIN
    SELECT -- Sélectionne
    EXISTS -- Teste l'existence de valeurs dans la sous-requête suivante (booléen)
        (
            SELECT * -- On sélectionne (tous les champs)
            FROM sditec_adresse.projet_voie -- De la table projet_voie
            WHERE (projet_voie.insee, projet_voie.type_voie, projet_voie.libelle) = (NEW.insee, NEW.type_voie, NEW.libelle) 
            -- Où les valeurs des champs insee, type_voie et libelle du nouvel objet ont un équivalent en base de données
            AND projet_voie.gid != NEW.gid -- Et où la valeur du champ gid est différente
        ) 
    INTO existedeja; -- Résultat enregistré dans la variable existedeja : "true" si une correspondance est trouvée sinon "false".
			
    IF existedeja THEN -- Si la variable existedeja est égale à true alors
    RAISE EXCEPTION 'Une voie portant ce nom existe déjà'; -- Ce message apparaît (pas dans X'MAP), et l'objet ne s'écrit pas en base de données
    ELSE RETURN NEW; -- Sinon on enregistre l'objet en base de données
    END IF; -- Fin de la condition
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.verif_voie_unique() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.verif_voie_unique() IS 
    'Fonction trigger permettant de bloquer la création d''une voie dans la table projet_voie lorsque celle-ci a les mêmes valeurs';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER verif_voie_unique_trigger 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_voie 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.verif_voie_unique();
    
COMMENT ON TRIGGER verif_voie_unique_trigger ON sditec_adresse.projet_voie IS 
    'Trigger déclenchant la fonction permettant de bloquer la création d''une voie dans la table projet_voie lorsque celle-ci a les mêmes valeurs';


-- ##################################################################################################################################################
-- ###                                        Construction par concaténation du champ identifiant_externe                                         ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.concat_identifiant_externe();

CREATE OR REPLACE FUNCTION sditec_adresse.concat_identifiant_externe()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.identifiant_externe := -- La valeur du champ identifiant_externe du nouvel objet sera égale à
        (SELECT CONCAT (right(NEW.insee,3),NEW.gid)); -- La concaténation de code INSEE (3 chiffres de droite) et du gid du nouvel objet
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.concat_identifiant_externe() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.concat_identifiant_externe() IS 
    'Fonction trigger permettant la concaténation de l''identifiant externe de la voie (insee,gid) dans le champ identifiant_externe';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER concat_identifiant_externe_trigger 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_voie 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.concat_identifiant_externe();
    
COMMENT ON TRIGGER concat_identifiant_externe_trigger ON sditec_adresse.projet_voie IS 
    'Trigger déclenchant la fonction permettant la concaténation de l''identifiant externe de la voie (insee,gid) dans le champ identifiant_externe';


-- ##################################################################################################################################################
-- ###                                            Concaténation du nom de la voie dans le champ ident                                             ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.concat_ident();

CREATE OR REPLACE FUNCTION sditec_adresse.concat_ident()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.ident := -- La valeur du champ ident du nouvel objet sera égale à
        (
            SELECT CONCAT (tv.libelle,' ',NEW.libelle) -- La concaténation du libellé du type de voie et du libellé de la voie du nouvel objet
            FROM sditec_adresse.type_voie AS tv -- On appelle la table type_voie tv pour aller chercher le libellé du type de voie
            WHERE NEW.type_voie = tv.code -- On choisit le type de voie correspondant au code du champ type_voie du nouvel objet
        );
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.concat_ident() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.concat_ident() IS 
    'Fonction trigger permettant la concaténation du nom complet de la voie (type_voie, ,libelle) dans le champ ident';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER concat_ident_trigger 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_voie 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.concat_ident();
    
COMMENT ON TRIGGER concat_ident_trigger ON sditec_adresse.projet_voie IS 
    'Trigger déclenchant la fonction permettant la concaténation du nom complet de la voie (type_voie, , libelle) dans le champ ident';


-- ##################################################################################################################################################
-- ###                                         Suppression des espaces précédant la valeur du champ ident                                         ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.trim_ident_projet_voie();

CREATE OR REPLACE FUNCTION sditec_adresse.trim_ident_projet_voie()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.ident := TRIM(NEW.ident); -- Suppression des espaces précédant la valeur du champ ident d'un nouvel objet
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.trim_ident_projet_voie() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.trim_ident_projet_voie() IS 
    'Fonction trigger permettant la suppression des espaces précédant la valeur du champ ident';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER trim_ident_projet_voie_trigger 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_voie 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.trim_ident_projet_voie();
    
COMMENT ON TRIGGER trim_ident_projet_voie_trigger ON sditec_adresse.projet_voie IS 
    'Trigger déclenchant la fonction permettant la suppression des espaces précédant la valeur du champ ident';


-- ##################################################################################################################################################
-- ###                              Mise en majuscule de la valeur des champs libelle, ident et identifiant_externe                               ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.majuscule_projet_voie();

CREATE OR REPLACE FUNCTION sditec_adresse.majuscule_projet_voie()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.libelle := UPPER(NEW.libelle); -- La valeur du champ libelle d'un nouvel objet sera égale à cette même valeur en majuscule
    NEW.ident := UPPER(NEW.ident); -- La valeur du champ ident d'un nouvel objet sera égale à cette même valeur en majuscule
    NEW.identifiant_externe := UPPER(NEW.identifiant_externe); -- La valeur du champ identifiant_externe d'un nouvel objet est mise en majuscule
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.majuscule_projet_voie() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.majuscule_projet_voie() IS 
    'Fonction trigger permettant la modification de la casse (majuscule) des champs libelle, ident et identifiant_externe';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER majuscule_projet_voie_trigger 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_voie 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.majuscule_projet_voie();
    
COMMENT ON TRIGGER majuscule_projet_voie_trigger ON sditec_adresse.projet_voie IS 
    'Trigger déclenchant la fonction permettant la modification de la casse (majuscule) des champs libelle, ident et identifiant_externe';


-- ##################################################################################################################################################
-- ###             Mise à jour du champ id_externe_voie (projet_numero) lorsque le champ identifiant_externe (projet_voie) est modifié            ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.maj_id_externe_voie_syncro();

CREATE OR REPLACE FUNCTION sditec_adresse.maj_id_externe_voie_syncro()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    UPDATE sditec_adresse.projet_numero -- Mise à jour de la table projet_numero
    SET id_externe_voie = UPPER(NEW.identifiant_externe) 
    -- id_externe_voie (projet_numero) prend la valeur en majuscule de identifiant_externe (projet_voie) de la voie modifiée
    WHERE id_externe_voie::text = OLD.identifiant_externe::text; -- Lorsque id_externe_voie est égal à l'ancien identifiant_externe de la voie
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.maj_id_externe_voie_syncro() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.maj_id_externe_voie_syncro() IS 
    'Fonction trigger permettant la mise à jour de id_externe_voie (projet_numero) lorsque identifiant_externe (projet_voie) est modifié';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER maj_id_externe_voie_syncro 
    BEFORE UPDATE
    ON sditec_adresse.projet_voie 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.maj_id_externe_voie_syncro();
    
COMMENT ON TRIGGER maj_id_externe_voie_syncro ON sditec_adresse.projet_voie IS 
    'Trigger déclenchant la fonction permettant la maj de id_externe_voie (projet_numero) lorsque identifiant_externe (projet_voie) est modifié';


-- ##################################################################################################################################################
-- ###                        Mise à jour du champ id_externe_voie (projet_numero) lorsqu'un objet projet_voie est supprimé                       ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.f_update_id_externe_voie_suppr_voie();

CREATE OR REPLACE FUNCTION sditec_adresse.f_update_id_externe_voie_suppr_voie()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    UPDATE sditec_adresse.projet_numero -- Mise à jour de projet_numero
    SET id_externe_voie = NULL -- Le champ id_externe_voie prend la valeur NULL
    WHERE (id_externe_voie) =(OLD.identifiant_externe); 
    -- Lorsqu'il existe une correspondance entre la valeur de id_externe_voie (projet_numero) et celle de identifiant_externe de la voie supprimée
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.f_update_id_externe_voie_suppr_voie() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.f_update_id_externe_voie_suppr_voie() IS 
    'Fonction trigger permettant la mise à jour du champ id_externe_voie (projet_numero) lorsqu''un objet projet_voie est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_voie_adresse_maj_pt_adresse 
    AFTER DELETE
    ON sditec_adresse.projet_voie 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.f_update_id_externe_voie_suppr_voie();
    
COMMENT ON TRIGGER t_after_d_voie_adresse_maj_pt_adresse ON sditec_adresse.projet_voie IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ id_externe_voie (projet_numero) lorsqu''un objet projet_voie est supprimé';


-- ##################################################################################################################################################
-- ###                   Mise à jour du champ cle_interop (projet_numero) lorsque le champ rivoli_atd (projet_voie) est modifié                   ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.maj_cle_interop_fantoir();

CREATE OR REPLACE FUNCTION sditec_adresse.maj_cle_interop_fantoir()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    UPDATE sditec_adresse.projet_numero -- Mise à jour de la table projet_numero
    SET cle_interop = 'test' -- cle_interop prend la valeur 'test'
    WHERE OLD.rivoli_atd != NEW.rivoli_atd -- Lorsque le champ rivoli_atd (projet_voie) change
    AND id_externe_voie = OLD.identifiant_externe; -- Et lorsqu'il y a correspondance entre la voie modifiée et les numéros
    RETURN NEW;
END;

-- NB : La valeur du champ cle_interop ne reste pas à 'test'. En effet ce trigger entraine la modification (update) de la table projet_numero. 
-- Ce qui déclenche la fonction trigger maj_cle_interop() qui lui permet de corriger correctement la valeur de ce champ.
    
$BODY$;

ALTER FUNCTION sditec_adresse.maj_cle_interop_fantoir() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.maj_cle_interop_fantoir() IS 
    'Fonction trigger permettant la mise à jour du champ cle_interop (projet_numero) lorsque le champ rivoli_atd (projet_voie) est modifié';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER maj_cle_interop_fantoir 
    AFTER UPDATE
    ON sditec_adresse.projet_voie 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.maj_cle_interop_fantoir();
    
COMMENT ON TRIGGER maj_cle_interop_fantoir ON sditec_adresse.projet_voie IS 
    'Trigger déclenchant la fonction permettant la mise à jour de cle_interop (projet_numero) lorsque le champ rivoli_atd (projet_voie) est modifié';

