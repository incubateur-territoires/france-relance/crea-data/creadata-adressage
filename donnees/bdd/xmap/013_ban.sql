-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/07/28 : DC / Création de la table

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Table géographique : BAN                                                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- Table: sditec_adresse.ban

-- DROP TABLE sditec_adresse.ban;

CREATE TABLE IF NOT EXISTS sditec_adresse.ban
(
    gid serial NOT NULL,
    insee character varying(80),
    datesig date,
    id character varying(80),
    nom_voie character varying(80),
    id_fantoir character varying(80),
    numero character varying(80),
    rep character varying(80),
    code_post character varying(80),
    alias character varying(80),
    nom_ld character varying(80),
    commune character varying(80),
    the_geom geometry(Point),
    code_insee_ancienne_commune character varying(5),
    nom_ancienne_commune character varying(80),
    libelle_acheminement character varying(80),
    nom_afnor character varying(80),
    source_position character varying(80),
    source_nom_voie character varying(80),
    certification_commune character varying(1),
    cad_parcelles character varying(254),
    latitude character varying(254),
    longitude character varying(254),
    x character varying(254),
    y character varying(254),
    CONSTRAINT ban_pkey PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE sditec_adresse.ban
    OWNER to sditecgrp;

GRANT ALL ON TABLE sditec_adresse.ban TO sditecgrp;

GRANT ALL ON TABLE sditec_adresse.ban TO simapdatagrp;

-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_adresse.ban IS '[ATD16] Table géographique des données provenant de la Base Adresse Nationale';

COMMENT ON COLUMN sditec_adresse.ban.gid IS '[ATD16] Identifiant unique généré automatiquement';
   
COMMENT ON COLUMN sditec_adresse.ban.insee IS '[ATD16] code insee de la commune';
COMMENT ON COLUMN sditec_adresse.ban.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN sditec_adresse.ban.id IS '[BAN] Clé d’interopérabilité telle que définie dans la spécification du format d''échange BAL 1.3. Lorsqu''aucun code FANTOIR n''est connu, un code transitoire composé de 6 caractères alpha-numériques est généré.';
COMMENT ON COLUMN sditec_adresse.ban.nom_voie IS '[BAN] Nom de la voie en minuscules accentuées';
COMMENT ON COLUMN sditec_adresse.ban.id_fantoir IS '[BAN] Identifiant FANTOIR de la voie, le cas échant. L''identifiant est préfixé par la commune de rattachement FANTOIR (commune actuelle ou commune ancienne)';
COMMENT ON COLUMN sditec_adresse.ban.numero IS '[BAN] Numéro de l’adresse dans la voie';
COMMENT ON COLUMN sditec_adresse.ban.rep IS '[BAN] Indice de répétition associé au numéro (par exemple bis, a…)';
COMMENT ON COLUMN sditec_adresse.ban.code_post IS '[BAN] Code postal du bureau de distribution de la voie';
COMMENT ON COLUMN sditec_adresse.ban.alias IS '[BAN] Vide';
COMMENT ON COLUMN sditec_adresse.ban.nom_ld IS '[BAN] Nom du lieu-dit de rattachement (ou autre type de toponyme)';
COMMENT ON COLUMN sditec_adresse.ban.commune IS '[BAN] Nom officiel de la commune actuelle';
COMMENT ON COLUMN sditec_adresse.ban.the_geom IS '[ATD16]geometry de l''objet';
COMMENT ON COLUMN sditec_adresse.ban.code_insee_ancienne_commune IS '[BAN] Code INSEE de l''ancienne commune sur laquelle est située l''adresse';
COMMENT ON COLUMN sditec_adresse.ban.nom_ancienne_commune IS '[BAN]Nom de l''ancienne commune sur laquelle est située l''adresse';
COMMENT ON COLUMN sditec_adresse.ban.libelle_acheminement IS '[BAN] Nom de la commune d’acheminement';
COMMENT ON COLUMN sditec_adresse.ban.nom_afnor IS '[BAN] Nom de la voie normalisé selon la norme postale';
COMMENT ON COLUMN sditec_adresse.ban.source_position IS '[BAN] 	Source de la position géographique. Valeurs possibles : (commune, cadastre, arcep, laposte, insee, sdis, inconnue)';
COMMENT ON COLUMN sditec_adresse.ban.source_nom_voie IS '[BAN]	Source du nom de la voie. Valeurs possibles : (commune, cadastre, arcep, laposte, insee, sdis, inconnue)';
COMMENT ON COLUMN sditec_adresse.ban.certification_commune IS '[BAN] Indique si l’adresse a été certifiée par la commune. Valeurs possibles : (1 pour oui, 0 pour non)';
COMMENT ON COLUMN sditec_adresse.ban.cad_parcelles IS '[BAN] Liste les identifiants des parcelles cadastrales auxquelles l’adresse est rattachée, si l''information est connue. Codage de l’identifiant sur 14 caractères. Séparateur |. Donnée en cours de fiabilisation';
COMMENT ON COLUMN sditec_adresse.ban.latitude IS '[BAN] Latitude en WGS-84';
COMMENT ON COLUMN sditec_adresse.ban.longitude IS '[BAN] Longitude en WGS-84';
COMMENT ON COLUMN sditec_adresse.ban.x IS '[BAN] Coordonnées cartographique en projection légale';
COMMENT ON COLUMN sditec_adresse.ban. y IS '[BAN] Coordonnées cartographique en projection légale';



-- ################################################################## Index ##################################################################
-- Index: ban_insee

-- DROP INDEX sditec_adresse.ban_insee;

CREATE INDEX ban_insee
    ON sditec_adresse.ban USING btree
    (insee ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: ban_the_geom_gist

-- DROP INDEX sditec_adresse.ban_the_geom_gist;

CREATE INDEX ban_the_geom_gist
    ON sditec_adresse.ban USING gist
    (the_geom)
    TABLESPACE pg_default;