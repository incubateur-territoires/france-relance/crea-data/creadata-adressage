-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/27 : SL / Migration du fichier sur Git
--                 . Ajout de la fonction majuscule_indice() et du trigger associé majuscule_indice_trigger
-- 2021/05/27 : SL / Ajout de la fonction majuscule_nom_ld() et du trigger associé majuscule_nom_ld_trigger
--                 . Ajout de la fonction f_majuscule_nom_occupant() et du trigger associé t_before_iu_majuscule_nom_occupant
--                 . Ajout de la fonction setXY_coord() et du trigger associé setXY_coord_trigger
--                 . Ajout de la fonction maj_refcad_prop_adresse() et du trigger associé maj_refcad_prop_adresse
--                 . Ajout de la fonction f_maj_commune_deleguee() et du trigger associé t_before_iu_maj_commune_deleguee
--                 . Ajout de la fonction maj_cle_interop() et du trigger associé maj_cle_interop
-- 2021/05/31 : SL / Modification de la fonction maj_refcad_prop_adresse(). Il faut mettre le SRID des nouveaux objets à 0
--                 . Modification de la fonction f_maj_commune_deleguee(). Il faut mettre le SRID des nouveaux objets à 0

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                     Fonctions triggers et triggers spécifiques à la table projet_numero                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                   Mise en forme en majuscule et sans espace de la valeur du champ indice                                   ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.majuscule_indice();

CREATE OR REPLACE FUNCTION sditec_adresse.majuscule_indice()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.indice := UPPER(TRIM(NEW.indice)); -- UPPER met en majuscule et TRIM enlève les espaces avant et après
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.majuscule_indice() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.majuscule_indice() IS 
    'Fonction trigger permettant la mise en forme en majuscule et sans espace de la valeur du champ indice';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER majuscule_indice_trigger 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_numero 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.majuscule_indice();
    
COMMENT ON TRIGGER majuscule_indice_trigger ON sditec_adresse.projet_numero IS 
    'Trigger déclenchant la fonction permettant la mise en forme en majuscule et sans espace de la valeur du champ indice';


-- ##################################################################################################################################################
-- ###                                   Mise en forme en majuscule et sans espace de la valeur du champ nom_ld                                   ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.majuscule_nom_ld();

CREATE OR REPLACE FUNCTION sditec_adresse.majuscule_nom_ld()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.nom_ld := UPPER(TRIM(NEW.nom_ld)); -- UPPER met en majuscule et TRIM enlève les espaces avant et après
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.majuscule_nom_ld() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.majuscule_nom_ld() IS 
    'Fonction trigger permettant la mise en forme en majuscule et sans espace de la valeur du champ nom_ld';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER majuscule_nom_ld_trigger 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_numero 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.majuscule_nom_ld();
    
COMMENT ON TRIGGER majuscule_nom_ld_trigger ON sditec_adresse.projet_numero IS 
    'Trigger déclenchant la fonction permettant la mise en forme en majuscule et sans espace de la valeur du champ nom_ld';


-- ##################################################################################################################################################
-- ###                                Mise en forme en majuscule et sans espace de la valeur du champ nom_occupant                                ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.f_majuscule_nom_occupant();

CREATE OR REPLACE FUNCTION sditec_adresse.f_majuscule_nom_occupant()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.nom_occupant := UPPER(TRIM(NEW.nom_occupant)); -- UPPER met en majuscule et TRIM enlève les espaces avant et après
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.f_majuscule_nom_occupant() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.f_majuscule_nom_occupant() IS 
    'Fonction trigger permettant la mise en forme en majuscule et sans espace de la valeur du champ nom_occupant';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_majuscule_nom_occupant 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_numero 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.f_majuscule_nom_occupant();
    
COMMENT ON TRIGGER t_before_iu_majuscule_nom_occupant ON sditec_adresse.projet_numero IS 
    'Trigger déclenchant la fonction permettant la mise en forme en majuscule et sans espace de la valeur du champ nom_occupant';


-- ##################################################################################################################################################
-- ###                                                 Récupération des coordonnées géographiques                                                 ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.setXY_coord();

CREATE OR REPLACE FUNCTION sditec_adresse.setXY_coord()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.x_wgs84 := ST_X(ST_Transform(ST_SetSRID(NEW.the_geom,3857),4326)); -- On récupère le X, on le définit en 3857, puis on le met en 4326
    NEW.y_wgs84 := ST_Y(ST_Transform(ST_SetSRID(NEW.the_geom,3857),4326)); -- On récupère le Y, on le définit en 3857, puis on le met en 4326
    NEW.x_cc46 := ST_X(ST_Transform(ST_SetSRID(NEW.the_geom,3857),3946)); -- On récupère le X, on le définit en 3857, puis on le met en 3946
    NEW.y_cc46 := ST_Y(ST_Transform(ST_SetSRID(NEW.the_geom,3857),3946)); -- On récupère le Y, on le définit en 3857, puis on le met en 3946
    NEW.x := ST_X(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- On récupère le X, on le définit en 3857, puis on le met en 2154
    NEW.y := ST_Y(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- On récupère le Y, on le définit en 3857, puis on le met en 2154
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.setXY_coord() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.setXY_coord() IS 
    'Fonction trigger permettant la récupération des coordonnées géographiques';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER setXY_coord_trigger 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_numero 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.setXY_coord();
    
COMMENT ON TRIGGER setXY_coord_trigger ON sditec_adresse.projet_numero IS 
    'Trigger déclenchant la fonction permettant la récupération des coordonnées géographiques';


-- ##################################################################################################################################################
-- ###                                                  Récupération des informations cadastrales                                                 ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.maj_refcad_prop_adresse();

CREATE OR REPLACE FUNCTION sditec_adresse.maj_refcad_prop_adresse()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    SELECT parc.ident, parc.nom  -- On sélectionne les champ ident (numéro de la parcelle) et nom (nom du 1er ayant-droit)
    FROM pci.v_geo_parcelle_encours AS parc -- De la table v_geo_parcelle_encours
    WHERE ST_Intersects(ST_SetSRID(NEW.the_geom,0), parc.the_geom) -- Lorsque le nouveau point d'adresse intersecte la parcelle
    -- Le ST_SetSRID à 0 est nécessaire dans Postgis 3. En effet les nouveaux objets arrivent en 4326.
    INTO NEW.parcelle, NEW.nom_prop; -- On rentre les données dans les champs parcelle et nom_prop du nouvel objet
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.maj_refcad_prop_adresse() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.maj_refcad_prop_adresse() IS 
    'Fonction trigger permettant la récupération des informations cadastrales';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER maj_refcad_prop_adresse 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_numero 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.maj_refcad_prop_adresse();
    
COMMENT ON TRIGGER maj_refcad_prop_adresse ON sditec_adresse.projet_numero IS 
    'Trigger déclenchant la fonction permettant la récupération des informations cadastrales';


-- ##################################################################################################################################################
-- ###                                 Récupération des informations concernant la commune déléguée (nom et insee)                                ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.f_maj_commune_deleguee();

CREATE OR REPLACE FUNCTION sditec_adresse.f_maj_commune_deleguee()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    SELECT RIGHT(histo.old_insee,5), histo.nom -- On sélectionne les champs old_insee (5 derniers chiffres) et nom
    FROM atd16_gestion.v_geo_commune_ancienne AS histo -- De la table des communes déléguées (v_geo_commune_ancienne)
    WHERE ST_Intersects(ST_SetSRID(NEW.the_geom,0), histo.the_geom) -- Lorsque le nouveau pt d'adresse intersecte la géométrie de la commune déléguée
    -- Le ST_SetSRID à 0 est nécessaire dans Postgis 3. En effet les nouveaux objets arrivent en 4326.
    INTO NEW.commune_deleguee_insee, NEW.commune_deleguee_nom; -- Et on écrit les valeurs dans les champs correspondant du nouvel objet
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.f_maj_commune_deleguee() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.f_maj_commune_deleguee() IS 
    'Fonction trigger permettant la récupération des informations concernant la commune déléguée (nom et insee)';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_commune_deleguee 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_numero 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.f_maj_commune_deleguee();
    
COMMENT ON TRIGGER t_before_iu_maj_commune_deleguee ON sditec_adresse.projet_numero IS 
    'Trigger déclenchant la fonction permettant la récupération des informations concernant la commune déléguée (nom et insee)';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ cle_interop                                                     ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION sditec_adresse.maj_cle_interop();

CREATE OR REPLACE FUNCTION sditec_adresse.maj_cle_interop()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF 
        NEW.indice IS NULL -- Si la valeur du champ indice du nouvel objet est NULL
    THEN
	    NEW.cle_interop :=  -- Alors la valeur du champ cle_interop du nouvel objet sera
		    (
		        SELECT CONCAT -- La concaténation
			    (RIGHT(NEW.insee,5),'_',pv.rivoli_atd,'_',TRIM(to_char(NEW.numero,'00000'))) 
			    -- De son code INSEE, du rivoli de sa voie et de son numéro d'adresse (sur 5 chiffres)
			    FROM sditec_adresse.projet_voie AS pv -- Appel de la table des voies (pour le champ rivoli_atd)
			    WHERE NEW.id_externe_voie = pv.identifiant_externe -- Lorsque le nouveau point d'adresse est associé à une voie
		    );
	ELSIF 
	    NEW.indice = '' -- Ou si la valeur du champ indice du nouvel objet est vide
    THEN
	    NEW.cle_interop := -- Alors la valeur du champ cle_interop du nouvel objet sera
		    (
		        SELECT CONCAT -- La concaténation
			    (RIGHT(NEW.insee,5),'_',pv.rivoli_atd,'_',TRIM(to_char(NEW.numero,'00000'))) 
			    -- De son code INSEE, du rivoli de sa voie et de son numéro d'adresse (sur 5 chiffres)
			    FROM sditec_adresse.projet_voie AS pv -- Appel de la table des voies (pour le champ rivoli_atd)
			    WHERE NEW.id_externe_voie = pv.identifiant_externe -- Lorsque le nouveau point d'adresse est associé à une voie
		    );
	ELSE
	    NEW.cle_interop := -- Sinon la valeur du champ cle_interop du nouvel objet sera
		    (
		        SELECT CONCAT -- La concaténation
			    (RIGHT(NEW.insee,5),'_',pv.rivoli_atd,'_',TRIM(to_char(NEW.numero,'00000')),'_',LOWER(NEW.indice)) 
			    -- De son code INSEE, du rivoli de sa voie, de son numéro d'adresse (sur 5 chiffres) et de son indice
			    FROM sditec_adresse.projet_voie AS pv -- Appel de la table des voies (pour le champ rivoli_atd)
			    WHERE NEW.id_externe_voie = pv.identifiant_externe -- Lorsque le nouveau point d'adresse est associé à une voie
		    );
	END IF;
	RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION sditec_adresse.maj_cle_interop() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.maj_cle_interop() IS 
    'Fonction trigger permettant la mise à jour du champ cle_interop';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER maj_cle_interop 
    BEFORE INSERT OR UPDATE
    ON sditec_adresse.projet_numero 
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_adresse.maj_cle_interop();
    
COMMENT ON TRIGGER maj_cle_interop ON sditec_adresse.projet_numero IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ cle_interop';

