-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/25 : SL / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Table non géographique : Domaine de valeur du type de numérotation                                      ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_adresse.type_numerotation

CREATE TABLE sditec_adresse.type_numerotation 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du type de numérotation
    libelle varchar(254), --[ATD16] Libellé du type de numérotation
    CONSTRAINT pk_type_numerotation PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE sditec_adresse.type_numerotation OWNER TO sditecgrp;

GRANT ALL ON TABLE sditec_adresse.type_numerotation TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_adresse.type_numerotation IS '[ATD16] Domaine de valeur des codes du type de numérotation';

COMMENT ON COLUMN sditec_adresse.type_numerotation.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN sditec_adresse.type_numerotation.code IS '[ATD16] Code du type de numérotation';
COMMENT ON COLUMN sditec_adresse.type_numerotation.libelle IS '[ATD16] Libellé du type de numérotation';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO sditec_adresse.type_numerotation 
    (code, libelle)
VALUES
    ('01', 'Séquentiel'),
    ('02', 'Métrique'),
    ('03', 'Décamétrique');

