-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/21 : SL / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) générique(s)                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP FUNCTION sditec_adresse.update_created_column(); 
-- DROP FUNCTION sditec_adresse.f_date_creation();

CREATE FUNCTION sditec_adresse.update_created_column() 
-- CREATE FUNCTION sditec_adresse.f_date_creation()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_creation = now(); -- À la création d'un objet, le champ date_creation prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION sditec_adresse.update_created_column() OWNER TO sditecgrp; 
-- ALTER FUNCTION sditec_adresse.f_date_creation() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.update_created_column() IS 'Fonction trigger permettant l''initialisation du champ date_creation';
-- COMMENT ON FUNCTION sditec_adresse.f_date_creation() IS 'Fonction trigger permettant l''initialisation du champ date_creation';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP FUNCTION sditec_adresse.update_modified_column(); 
-- DROP FUNCTION sditec_adresse.f_date_maj();

CREATE FUNCTION sditec_adresse.update_modified_column() 
-- CREATE FUNCTION sditec_adresse.f_date_maj()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_maj = now(); -- À la modification d'un objet, le champ date_maj prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION sditec_adresse.update_modified_column() OWNER TO sditecgrp;
-- ALTER FUNCTION sditec_adresse.f_date_maj() OWNER TO sditecgrp;

COMMENT ON FUNCTION sditec_adresse.update_modified_column() IS 'Fonction trigger permettant la mise à jour du champ date_maj';
-- COMMENT ON FUNCTION sditec_adresse.f_date_maj() IS 'Fonction trigger permettant la mise à jour du champ date_maj';

