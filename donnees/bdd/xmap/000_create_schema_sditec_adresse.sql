-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/21 : SL / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                 SCHEMA : Gestion de l'adressage des communes                                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP SCHEMA IF EXISTS sditec_adresse;

CREATE SCHEMA sditec_adresse;
ALTER SCHEMA sditec_adresse OWNER TO sditecgrp;
COMMENT ON SCHEMA sditec_adresse IS 'Gestion de l''adressage des communes';


-- ########################################################### Changement du nom du schéma ##########################################################

-- DROP SCHEMA IF EXISTS atd16_adresse;

-- CREATE SCHEMA atd16_adresse;
-- ALTER SCHEMA atd16_adresse OWNER TO sditecgrp;
-- COMMENT ON SCHEMA atd16_adresse IS 'Gestion de l'adressage des communes';

