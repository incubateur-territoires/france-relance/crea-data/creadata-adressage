# Projet permettant la gestion des noms de voies et adresses

Ensemble des éléments constituant la démarche d'accompagement de l'ATD16 en maitère de qualification d'adressage.

# Principe général

Blabla
* modèle de données adapté au logiciel X'MAP
* MCD

# Ressources

* [Les bonnes pratiques dans GitLab](https://git.atd16.fr/cesig/gestion-de-git/-/blob/master/les_bonnes_pratiques_gitlab.md)
* [Modèle de données de l'AITF v 1.2 novembre 2020](https://aitf-sig-topo.github.io/voies-adresses/files/AITF_SIG_Topo_Format_Base_Adresse_Locale_v1.2.pdf)
* [Modèle de données de l'AITF v 1.3 novembre 2021](https://aitf-sig-topo.github.io/voies-adresses/files/AITF_SIG_Topo_Format_Base_Adresse_Locale_v1.3.pdf)
* [Schéma applicatif des tables d'adressage](ressources/schema_applicatif_adressage.png)
* [Procédure diffusion des données sur google](https://atd16.sharepoint.com/:w:/s/cesig/EQk6lRKYSxBEt1F31MWcps0BHZc6yJa_LlMYKZ_R3kCg0Q?e=mbZCHF)
* [Procédure extraction des données](https://atd16.sharepoint.com/:w:/s/cesig/Eb4WrHG4ikBCgIdyL7KxHdIBt_Rrftp587wemwWzk02-7A?e=CdjMvP)
* [Procédure mise à jour des voies nommées sur Pigma](https://atd16.sharepoint.com/:w:/s/cesig/EduY9bA2mfFKqv9ZnbP1py4BBb2N_bezM8LFrModsST3ww?e=BZqK5n)
* [Procédure mise à jour des adresses vars la BAN](https://atd16.sharepoint.com/:w:/s/cesig/EXdoGUGeWMlHhfuuaOXLWuIB-zUtfMrlhQNGXqs2uFXAgg?e=X78lkg)
* [Procédure interne de normalisation de l'adressage](ressources/procedure_interne_normalisation_adressage.drawio_1.pdf)